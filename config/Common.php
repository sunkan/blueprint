<?php

namespace Blueprint\_Config;

use Aura\Di\Container;
use Blueprint\Helper\Resolver;
use Blueprint\Helper\ResolverList;

class Common
{
    const VERSION = '2.4.0';

    public function define(Container $di)
    {
        $di->set('blueprint:resolver_list', function () use ($di) {
            $resolver = new Resolver(function ($cls) use ($di) {
                return $di->newInstance($cls);
            });
            $resolver->addNs('Blueprint\DesignHelper');
            $list = $di->newInstance(ResolverList::class);
            $list[] = $resolver;
            return $list;
        });

        $di->types[ResolverList::class] = $di->lazyGet('blueprint:resolver_list');
    }
}
