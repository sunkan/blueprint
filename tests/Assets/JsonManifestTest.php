<?php

namespace Blueprint\Assets;

use PHPUnit\Framework\TestCase;

class JsonManifestTest extends TestCase
{
    public function testMissingAssetFile()
    {
        $manifest = new JsonManifest('');
        $file = 'main.css';

        $this->assertSame($file, $manifest->getFileName($file));
    }

    public function testWithAssetFile()
    {
        $assetFile = dirname(__DIR__).'/resources/assets.json';
        $manifest = new JsonManifest($assetFile);
        $file = 'main.css';

        $this->assertSame('main-098af21e.css', $manifest->getFileName($file));
    }

    public function testMissingAssetInAssetFile()
    {
        $assetFile = dirname(__DIR__).'/resources/assets.json';
        $manifest = new JsonManifest($assetFile);
        $file = 'main.js';

        $this->assertSame('main.js', $manifest->getFileName($file));
    }
}
