<?php

namespace Blueprint\Assets;

use Actus\Path;
use PHPUnit\Framework\TestCase;

class FinderTest extends TestCase
{
    protected function getManifest()
    {
        $assetFile = dirname(__DIR__).'/resources/assets.json';
        return new JsonManifest($assetFile);
    }

    protected function getActus()
    {
        $_SERVER['HTTP_HOST'] = 'error';

        $path = new Path();
        $path->setRoot(dirname(__DIR__));
        $path->setRootDomains([
            'styles' => 'static.stylewish.me',
            'js' => 'static.stylewish.me',
        ]);
        $path->set('js', dirname(__DIR__) . '/dist/scripts');
        $path->set('styles', dirname(__DIR__) . '/dist/styles');

        return $path;
    }

    public function testWithActus()
    {
        $finder = new Finder($this->getManifest(), $this->getActus());
        $domain = "//static.stylewish.me";

        $this->assertSame($domain.'/dist/styles/main-098af21e.css', $finder->find('styles:main.css'));
        $this->assertSame($domain.'/dist/scripts/main.js', $finder->find('js:main.js'));
    }
    
    public function testFind()
    {
        $finder = new Finder($this->getManifest());

        $this->assertSame('styles/main-098af21e.css', $finder->find('styles/main.css'));
        $this->assertSame('scripts/main.js', $finder->find('scripts/main.js'));
    }
    
}
