<?php

namespace Blueprint\Helper;

use Blueprint\Exception\HelperNotFoundException;
use Blueprint\TemplateInterface;
use PHPUnit\Framework\TestCase;

class ResolverListTest extends TestCase
{
    public function testEmptyList()
    {
        /** @var TemplateInterface $engine */
        $engine = $this->getMockForAbstractClass(TemplateInterface::class);

        $this->expectException(HelperNotFoundException::class);
        $list = new ResolverList();

        $list->resolve('test', $engine);
    }

    public function testResolve()
    {
        /** @var TemplateInterface $engine */
        $engine = $this->getMockForAbstractClass(TemplateInterface::class);

        $helper = $this->getMockForAbstractClass(AbstractHelper::class);

        $resolver = $this->getMockForAbstractClass(ResolverInterface::class);
        $resolver->expects($this->any())->method('resolve')->will($this->returnValue($helper));
        $list = new ResolverList();
        $list[] = $resolver;

        $returnedHelper = $list->resolve('test', $engine);

        $this->assertSame($helper, $returnedHelper);
    }

    public function testResolveLifo()
    {
        /** @var TemplateInterface $engine */
        $engine = $this->getMockForAbstractClass(TemplateInterface::class);

        $helper = $this->getMockForAbstractClass(AbstractHelper::class);
        $helper2 = $this->getMockForAbstractClass(AbstractHelper::class);

        $resolver = $this->getMockForAbstractClass(ResolverInterface::class);
        $resolver->expects($this->exactly(0))->method('resolve')->will($this->returnValue($helper));

        $resolver2 = $this->getMockForAbstractClass(ResolverInterface::class);
        $resolver2->expects($this->any())->method('resolve')->will($this->returnValue($helper2));

        $list = new ResolverList([], true);
        $list[] = $resolver;
        $list[] = $resolver2;

        $returnedHelper = $list->resolve('test', $engine);

        $this->assertSame($helper2, $returnedHelper);
    }
}
