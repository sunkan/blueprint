<?php

namespace Blueprint;

use Actus\Path;

class ActusFinderTest extends DefaultFinderTest
{

    protected function setUp()
    {
        $actus = new Path();
        $actus->setRoot(__DIR__);
        $actus->set('template', __DIR__ . '/resources/folder2');
        $actus->set('template', __DIR__ . '/resources/');

        $this->finder = new ActusFinder(
            $actus,
            [
                __DIR__.'/resources/',
                __DIR__.'/resources/folder2/',
            ]
        );
    }

    public function testFindWithActus()
    {
        $tpl = $this->finder->findTemplate('template:tpl.php');
        $this->assertSame(__DIR__.'/resources/tpl.php', $tpl);
    }

    public function testFindWithActusWithoutExtension()
    {
        $tpl = $this->finder->findTemplate('template:folder2/tpl');
        $this->assertSame(__DIR__.'/resources/folder2/tpl.php', $tpl);
    }

    public function testFindWithActusWithCustomType()
    {
        $tpl = $this->finder->findTemplate('template:tpl', 'tmp');
        $this->assertSame(__DIR__.'/resources/folder2/tpl.tmp.php', $tpl);
    }
}
