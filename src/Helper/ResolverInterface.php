<?php declare(strict_types=1);

namespace Blueprint\Helper;

use Blueprint\Exception\HelperNotFoundException;
use Blueprint\TemplateInterface;

interface ResolverInterface
{
    /**
     * @param string $method
     * @param TemplateInterface $template
     * @return HelperInterface
     * @throws HelperNotFoundException
     */
    public function resolve(string $method, TemplateInterface $template): HelperInterface;
}
