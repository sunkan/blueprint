<?php declare(strict_types=1);

namespace Blueprint\Helper;

use Blueprint\Exception\HelperNotFoundException;
use Blueprint\TemplateInterface;

class Resolver implements ResolverInterface
{
    protected $map = [];
    protected $ns = [];

    protected $resolver;

    public function __construct(callable $resolver)
    {
        $this->resolver = $resolver;
    }

    public function setNs(array $ns): void
    {
        $this->ns = $ns;
    }

    public function addNs($ns): void
    {
        $this->ns[] = $ns;
    }

    /**
     * @param HelperInterface $class
     */
    public function addClass(HelperInterface $class): void
    {
        $this->map[$class->getName()] = $class;
    }

    /**
     * @param string $key
     * @param callable $func
     */
    public function addFunction(string $key, callable $func): void
    {
        $this->map[$key] = new ClosureHelper($func);
    }

    /**
     * @param string $method
     * @param TemplateInterface $template
     * @return HelperInterface
     * @throws HelperNotFoundException
     */
    public function resolve(string $method, TemplateInterface $template): HelperInterface
    {
        if (!isset($this->map[$method])) {
            $helper = null;
            $resolver = $this->resolver;

            foreach (array_reverse($this->ns) as $ns) {
                try {
                    $helper = $resolver($ns.'\\'.ucfirst($method));
                    if ($helper instanceof HelperInterface) {
                        $helper->setTemplate($template);
                        break;
                    }
                } catch (\Exception $e) {
                    //helper not found continue looking
                    $helper = false;
                }
            }
            $this->map[$method] = $helper;
        }

        if (!$this->map[$method]) {
            throw new HelperNotFoundException("Could't find helper: ".$method);
        }
        return $this->map[$method];
    }
}
