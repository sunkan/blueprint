<?php

namespace Blueprint\Helper;

final class ClosureHelper extends AbstractHelper
{
    private $clb;

    public function __construct(callable $clb)
    {
        $this->clb = $clb;
    }

    public function getName(): string
    {
        return 'closureHelper';
    }

    public function run(array $args)
    {
        $clb = $this->clb;
        return $clb($args);
    }

    public function getCallback(): callable
    {
        return $this->clb;
    }
}
