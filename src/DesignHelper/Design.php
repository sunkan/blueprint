<?php declare(strict_types=1);

namespace Blueprint\DesignHelper;

use Blueprint\FinderInterface;
use Blueprint\Helper\AbstractHelper;

class Design extends AbstractHelper
{
    /** @var string */
    protected $type;

    /** @var FinderInterface */
    protected $templateFinder;

    public function __construct(FinderInterface $finder)
    {
        $this->templateFinder = $finder;
    }

    public function getName(): string
    {
        return 'design';
    }

    public function run(array $args): self
    {
        return $this;
    }

    public function setLayout(string $type = null): void
    {
        $this->type = $type;
    }

    private function render(?string $file, string $type = null): string
    {
        $view = $this->templateEngine;
        $file = $this->templateFinder->findTemplate('layout/' . $file, $type ?? $this->type);

        return include $file;
    }

    public function header(string $type = null): string
    {
        return $this->render('header', $type);
    }

    public function footer(string $type = null): string
    {
        return $this->render('footer', $type);
    }
}
