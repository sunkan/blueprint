<?php declare(strict_types=1);

namespace Blueprint\DesignHelper;

use Blueprint\Helper\AbstractHelper;

final class Link extends AbstractHelper
{
    /** @var array */
    private $links = [];

    public function getName(): string
    {
        return 'link';
    }

    public function run(array $args): self
    {
        $argc = count($args);
        if ($argc === 0) {
            return $this;
        }
        return $this->add($args);
    }

    /**
     * Add link tag to header
     *
     * @param array $spec
     * @return $this
     */
    public function add($spec): self
    {
        $this->links[] = $spec;

        return $this;
    }

    /**
     * Set link tag. If rel already exist it will be replaced
     *
     * @param array $spec
     * @return $this
     */
    public function set($spec): self
    {
        for ($count = count($this->links)-1; $count >= 0; $count--) {
            $link = $this->links[$count];
            if ($link['rel'] === $spec['rel']) {
                unset($this->links[$count]);
            }
        }
        return $this->add($spec);
    }

    /**
     * Get link tags of type
     *
     * @param string $type
     * @return array
     */
    public function get($type): array
    {
        $return = array();
        foreach ($this->links as $obj) {
            if ($obj['type'] === $type) {
                $return[] = $obj['href'];
            }
        }
        return $return;
    }

    /**
     * Set canonical
     *
     * @param string $href
     * @return $this
     */
    public function setCanonical($href): self
    {
        $spec['rel'] = 'canonical';
        $spec['href'] = $href;

        return $this->set($spec);
    }

    /**
     * Create a new link css tag
     *
     * @param string $href
     * @param string $media
     * @return $this
     */
    public function addCss($href, $media = 'screen'): self
    {
        $spec['rel'] = 'stylesheet';
        $spec['type'] = 'text/css';
        $spec['href'] = $href;
        $spec['media'] = $media;

        return $this->add($spec);
    }

    /**
     * Renders link tags returns a string with all tags
     *
     * @param callable $func
     * @return string
     */
    public function render(callable $func = null): string
    {
        if ($func === null) {
            $func = static function ($obj) {
                $tpl = '<link %s/>' . "\n";
                $tpl2 = '%s="%s" ';
                $str = '';
                foreach ($obj as $key => $value) {
                    $str .= sprintf($tpl2, $key, $value);
                }
                return sprintf($tpl, $str);
            };
        }
        return implode('', array_map($func, $this->links));
    }

    /**
     * @see Link::render()
     * @return string
     */
    public function __toString()
    {
        return $this->render();
    }
}
