<?php declare(strict_types=1);

namespace Blueprint\DesignHelper;

use Blueprint\Helper\AbstractHelper;

final class Title extends AbstractHelper
{
    /** @var string */
    private $title = '';

    public function getName(): string
    {
        return 'title';
    }

    public function run(array $argv): self
    {
        $argc = count($argv);
        if ($argc === 0) {
            return $this;
        }
        return $this->set($argv[0]);
    }

    public function set(string $spec): self
    {
        $this->title = $spec;

        return $this;
    }

    public function render(): string
    {
        return $this->title;
    }

    public function __toString()
    {
        return $this->render();
    }
}
