<?php declare(strict_types=1);

namespace Blueprint\DesignHelper;

use Blueprint\Helper\AbstractHelper;
use Blueprint\Assets\FinderInterface as AssetFinder;

final class Asset extends AbstractHelper
{
    private $finder;

    public function __construct(AssetFinder $finder)
    {
        $this->finder = $finder;
    }

    public function run(array $args)
    {
        if (!is_string($args[0])) {
            throw new \InvalidArgumentException('Expected path to be string');
        }

        return new class($this->finder->find($args[0], true), $this->finder->find($args[0])) {
            private $file;
            private $url;

            public function __construct(?string $file, ?string $url)
            {
                $this->file = $file;
                $this->url = $url;
            }

            public function inline(): string
            {
                if (file_exists($this->file)) {
                    return file_get_contents($this->file);
                }
                return '';
            }

            public function __toString()
            {
                return (string)$this->url;
            }
        };
    }

    public function getName(): string
    {
        return 'asset';
    }
}
