<?php declare(strict_types=1);

namespace Blueprint\DesignHelper;

use Blueprint\Helper\AbstractHelper;

final class Script extends AbstractHelper
{
    public const PRODUCTION = 'prod';
    public const DEVELOPMENT = 'dev';

    private $mode = self::DEVELOPMENT;

    /** @var array */
    private $scripts = [];

    private $on = [
        'load' => '',
        'config' => ''
    ];

    public function getName(): string
    {
        return 'script';
    }

    /**
     * $argv[0] = src
     * $argv[1] = type
     * $argv[2] = pos
     *
     * @param array $argv
     * @return $this
     */
    public function run(array $argv): self
    {
        $argc = count($argv);
        if ($argc === 0) {
            return $this;
        }
        $src = $argv[0];
        $type = $argv[1] ?? 'text/javascript';
        $pos = $argv[2] ?? 'bottom';
        return $this->add($src, $type, $pos);
    }

    /**
     * @param string $src
     * @param string $type
     * @param string $pos
     * @return $this
     */
    public function add(string $src, string $type = 'text/javascript', string $pos = 'bottom'): self
    {
        $mode = false;
        if ($type === self::DEVELOPMENT) {
            $mode = self::DEVELOPMENT;
            $type = 'text/javascript';
        } elseif ($type === self::PRODUCTION) {
            $mode = self::PRODUCTION;
            $type = 'text/javascript';
        }
        $obj = new \stdClass;
        $obj->src = $src;
        $obj->type = $type;
        $obj->position = $pos;
        $obj->mode = $mode;
        $this->scripts[md5($src)] = $obj;

        return $this;
    }

    /**
     * Start ob_start every thing after this call will be inserted into script tag
     */
    public function start(): void
    {
        ob_start();
        echo "\n(function(){\n";
    }

    /**
     * gets script content from when ->start() was called
     *
     * @param string $var set a name to script content
     */
    public function end(string $var = 'load'): void
    {
        echo "\n})();\n";
        $this->on[$var] .= ob_get_clean();
    }

    /**
     * @param string $pos
     * @return boolean
     */
    public function has(string $pos): bool
    {
        $scripts = $this->scripts;
        $scripts = array_filter($scripts, static function ($obj) use ($pos) {
            return ($obj->position === $pos);
        });
        return count($scripts) > 0;
    }

    /**
     * @param string $pos
     * @return array of scripts
     */
    public function get($pos): array
    {
        $return = [];
        foreach ($this->scripts as $obj) {
            if ($obj->position === $pos) {
                $return[] = $obj->src;
            }
        }
        return $return;
    }

    public function setMode(string $mode): void
    {
        if (!in_array($mode, [self::DEVELOPMENT, self::PRODUCTION], true)) {
            throw new \InvalidArgumentException('Invalid mode');
        }
        $this->mode = $mode;
    }

    public function render($func = null, string $pos = 'all'): string
    {
        if (is_string($func) && !is_callable($func)) {
            $pos = strtolower($func);
        }

        if (isset($this->on[substr($pos, 2)])) {
            return $this->on[substr($pos, 2)];
        }

        $scripts = $this->scripts;
        if ($pos !== 'all') {
            $scripts = array_filter($scripts, static function ($obj) use ($pos) {
                return $obj->position === $pos;
            });
        }

        if (!is_callable($func)) {
            $func = function ($obj) {
                if ($obj->mode) {
                    if ($this->hasTemplate() && $this->getTemplate()->site_mode !== $obj->mode) {
                        return '';
                    }

                    if ($this->mode !== $obj->mode) {
                        return '';
                    }
                }
                $tpl = '<script type="%s" src="%s"></script>' . "\n";
                return sprintf($tpl, $obj->type, $obj->src);
            };
        }

        return implode('', array_map($func, $scripts));
    }
}
