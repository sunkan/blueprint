<?php declare(strict_types=1);

namespace Blueprint;

interface TemplateInterface
{
    /**
     * @return string Rendered template
     */
    public function render(): string;

    /**
     * @param string|array $key
     * @param mixed $value
     */
    public function assign($key, $value = null);
}
