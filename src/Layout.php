<?php declare(strict_types=1);

namespace Blueprint;

class Layout extends Extended
{
    /** @var TemplateInterface */
    protected $content;

    /**
     * @param TemplateInterface $tpl
     * @return $this
     */
    public function setContent(TemplateInterface $tpl): self
    {
        $this->content = $tpl;

        return $this;
    }

    /**
     * @return TemplateInterface
     */
    public function getContent(): TemplateInterface
    {
        return $this->content;
    }

    /**
     * @param string|null $file
     * @return string
     */
    public function render(?string $file = null): string
    {
        if ($this->content instanceof TemplateInterface) {
            $this->assign('content', $this->content->render());
        }
        return parent::render($file);
    }

    public function assign($spec, $value = null): bool
    {
        if ($this->content instanceof TemplateInterface) {
            $this->content->assign($spec, $value);
        }
        return parent::assign($spec, $value);
    }
}
