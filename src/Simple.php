<?php declare(strict_types=1);

namespace Blueprint;

use Blueprint\Exception\HelperNotFoundException;
use Blueprint\Helper\ResolverList;

class Simple implements TemplateInterface, CallableInterface
{
    protected $payload = [];
    protected $resolverList;

    public function __construct(ResolverList $resolverList)
    {
        $this->resolverList = $resolverList;
    }

    public function addResolver(Helper\ResolverInterface $resolver)
    {
        $this->resolverList[] = $resolver;
    }

    /**
     * @param array<Helper\ResolverInterface> $resolvers
     */
    public function setResolvers(array $resolvers)
    {
        $this->resolverList = [];
        foreach ($resolvers as $resolver) {
            $this->addResolver($resolver);
        }
    }

    /**
     * @param string $method
     * @param array $args
     * @return mixed
     */
    public function callHelper(string $method, array $args)
    {
        try {
            $helper = $this->resolverList->resolve($method, $this);
            return $helper->run($args);
        } catch (HelperNotFoundException $hnfe) {
            return false;
        }
    }

    /**
     * @param string $method
     * @param array $args
     * @return mixed
     */
    public function __call($method, $args)
    {
        return $this->callHelper($method, $args);
    }

    public function __get(string $key)
    {
        return $this->payload[$key] ?? null;
    }

    /**
     * @param string $var
     * @param mixed $value
     */
    public function __set($var, $value)
    {
        $this->payload[$var] = $value;
    }

    public function __isset($name)
    {
        return isset($this->payload[$name]);
    }

    /**
     * @param string|array|object $spec
     * @param mixed $value
     * @return boolean
     */
    public function assign($spec, $value = null): bool
    {
        if (is_string($spec) && $spec[0] !== "_") {
            $this->payload[$spec] = $value;
            return true;
        }
        if (is_array($spec)) {
            foreach ($spec as $key => $val) {
                if ($key[0] !== "_") {
                    $this->payload[$key] = $val;
                }
            }
            return true;
        }
        if (is_object($spec)) {
            foreach (get_object_vars($spec) as $key => $val) {
                if ($key[0] !== '_') {
                    $this->payload[$key] = $val;
                }
            }
            return true;
        }
        return false;
    }

    /**
     * @param string|null $file
     * @return string
     */
    public function render(?string $file = null): string
    {
        try {
            $__currentErrorReporting = error_reporting(E_ERROR | E_WARNING | E_PARSE);
            ob_start();
            //make $view available in all template scopes
            $view = $this;
            extract($this->payload);
            require $file;

            $data = ob_get_clean();
            error_reporting($__currentErrorReporting);
            return $data;
        } catch (\Exception $e) {
            error_reporting($__currentErrorReporting);
            return '';
        }
    }
}
