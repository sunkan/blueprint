<?php declare(strict_types=1);

namespace Blueprint;

use Actus\Path;
use Blueprint\Exception\TemplateNotFoundException;

class ActusFinder extends DefaultFinder
{
    protected $actus;

    public function __construct(Path $actus, array $paths = [])
    {
        parent::__construct($paths);
        $this->actus = $actus;
    }

    /**
     * @param string $file
     * @param null|string $type
     * @return string
     * @throws TemplateNotFoundException
     */
    public function findTemplate(string $file, ?string $type = null): string
    {
        if (strpos($file, ':')) {
            try {
                $cleanFile = $this->cleanFilename($file, $type);
                $tpl = $this->actus->get($cleanFile);
                if ($tpl) {
                    return $tpl;
                }
            } catch (\Exception $e) {}


            $file = str_replace(':', '/', $file);
        }
        return parent::findTemplate($file, $type);
    }
}
