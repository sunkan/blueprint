<?php declare(strict_types=1);

namespace Blueprint\Assets;

final class JsonManifest
{
    private $manifest;

    public function __construct(string $manifestPath)
    {
        if (file_exists($manifestPath)) {
            $this->manifest = json_decode(file_get_contents($manifestPath), true);
        } else {
            $this->manifest = [];
        }
    }

    public function getFileName(string $file): ?string
    {
        $collection = $this->manifest;
        if (isset($collection[$file])) {
            return $collection[$file];
        }
        foreach (explode('.', $file) as $segment) {
            if (!isset($collection[$segment])) {
                return $file;
            }

            $collection = $collection[$segment];
        }

        return is_string($collection) ? $collection : null;
    }
}
