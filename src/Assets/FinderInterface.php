<?php declare(strict_types=1);

namespace Blueprint\Assets;

interface FinderInterface
{
    public function find(string $path, bool $local = false): ?string;
}
