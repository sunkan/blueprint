<?php declare(strict_types=1);

namespace Blueprint\Assets;

use Actus\Path;

final class Finder implements FinderInterface
{
    private $pathCache = [];
    private $manifest;
    private $pathFinder;

    public function __construct(JsonManifest $manifest, ?Path $path = null)
    {
        $this->manifest = $manifest;
        $this->pathFinder = $path;
    }

    public function find(string $path, bool $local = false): ?string
    {
        if (strpos($path, ':')) {
            [, $basename] = explode(':', $path, 2);
            if ($this->pathFinder instanceof Path) {
                $this->pathCache[$path . '_local'] = $this->pathCache[$path . '_local'] ?? $this->pathFinder->get($path);

                if ($local) {
                    return $this->pathCache[$path . '_local'];
                }

                $this->pathCache[$path . '_remote'] =
                    $this->pathCache[$path . '_remote'] ??
                    $this->pathFinder->url($this->pathCache[$path . '_local'] ?? $path);

                return str_replace(
                    $basename,
                    $this->manifest->getFileName($basename),
                    $this->pathCache[$path . '_remote']
                );
            }
            $path = $basename;
        }
        $directory = dirname($path) . '/';
        $basename = basename($path);
        $filename = $this->manifest->getFileName($basename);

        return $directory . $filename;
    }
}
