<?php declare(strict_types=1);

namespace Blueprint;

interface CallableInterface
{
    public function callHelper(string $method, array $args);
}
