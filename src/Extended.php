<?php declare(strict_types=1);

namespace Blueprint;

use Blueprint\Helper\ResolverList;

class Extended extends Simple
{
    /** @var FinderInterface */
    protected $templateFinder;

    /** @var string */
    protected $templateFile;

    public function __construct(FinderInterface $finder, ResolverList $resolverList)
    {
        parent::__construct($resolverList);
        $this->templateFinder = $finder;
    }

    public function setTemplateFinder(FinderInterface $finder): void
    {
        $this->templateFinder = $finder;
    }

    public function setTemplate($tpl): void
    {
        $this->templateFile = $tpl;
    }

    public function getTemplate(): string
    {
        return $this->templateFile;
    }

    public function render(?string $file = null): string
    {
        return parent::render($this->templateFinder->findTemplate($file ?? $this->templateFile));
    }
}
