<?php declare(strict_types=1);

namespace Blueprint;

use Blueprint\Exception\TemplateNotFoundException;

interface FinderInterface
{
    /**
     * @param string $file
     * @param null|string $type
     * @return string
     * @throws TemplateNotFoundException
     */
    public function findTemplate(string $file, ?string $type = null): string;
}
