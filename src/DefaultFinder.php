<?php declare(strict_types=1);

namespace Blueprint;

use Blueprint\Exception\TemplateNotFoundException;

class DefaultFinder implements FinderInterface
{
    protected $paths = [];

    public function __construct(array $paths = [])
    {
        foreach ($paths as $path) {
            $this->addPath($path);
        }
    }

    public function addPath(string $path): void
    {
        if (!in_array($path, $this->paths, true)) {
            $this->paths[] = rtrim($path, DIRECTORY_SEPARATOR);
        }
    }

    /**
     * @param string $file
     * @param null|string $type
     * @return string
     * @throws TemplateNotFoundException
     */
    public function findTemplate(string $file, ?string $type = null): string
    {
        $cleanFile = $this->cleanFilename($file, $type);
        foreach (array_reverse($this->paths) as $path) {
            $filePath = $path . DIRECTORY_SEPARATOR . $cleanFile;
            if (file_exists($filePath)) {
                return $filePath;
            }
        }

        throw new TemplateNotFoundException($file . ':' . $type);
    }

    protected function cleanFilename(string $file, ?string $type): string
    {
        $needExtension = substr($file, -4) !== '.php';
        return $file . ($type === null ? '' : '.' . $type) . ($needExtension?'.php':'');
    }
}
